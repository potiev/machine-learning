<?php

namespace app\commands;
use app\components\StemmerComponent;
use Yii;
use yii\base\ErrorException;
use app\components\StructureParserComponent;
use app\components\ContentParserComponent;
use app\models\Dictionary;
use yii\db\Query;
use yii\helpers\ArrayHelper;


class StartController extends BasicController
{
    const ALL_DOCUMENTS_COUNT = 80000;
    const MIN_EXP = 0.0001;
    public $structure_filename = "data/structure.rdf.u8";
    public $content_filename = "data/content.rdf.u8";
    public $structure_filename_gz = "data/structure.rdf.u8.gz";
    public $content_filename_gz = "data/content.rdf.u8.gz";
    public $download_sf_url = 'http://rdf.dmoz.org/rdf/structure.rdf.u8.gz';
    public $download_cf_url = 'http://rdf.dmoz.org/rdf/content.rdf.u8.gz';
    public $content_descriptions = [];

    public $structure_parser;
    public $content_parser;


    /**
     * Deletes from file garbage characters
     * @param $filename
     */
    public  function cleanFile($filename)
    {
        $this->printToConsole("Cleaning $filename");
        $this->printToConsole("This could take some time!");
        $this->printToConsole("Status: ", false);

        //Open 2 file pointers - one for read and one for write
        $fp = fopen($filename, 'r');
        $writeFp = fopen("clean_$filename" , 'w');

        if ($fp == false) {
            throw new ErrorException("Can not open $filename");
        }

        if ($writeFp == false) {
            throw new ErrorException("Can not open clean_$filename");
        }

        //The things we want to clean
        $pattern = array('=<[biu]>(.*)</[biu]>=i', '=&(?!amp;)=i', '=<strong>(.*)</strong>=i', '=[\x00-\x08\x0b-\x0c\x0e-\x1f]=');

        //The corrections (the stuff we insert)
        $contractions = array("$1", '&amp;', "$1", '');

        //Continue until you have reached end of file
        while (!feof($fp))
        {
            $dirtyData = fgets($fp, 8192); //Get some dirty data
            $cleanData = preg_replace($pattern, $contractions, $dirtyData);
            //echo($cleanData);

            fwrite($writeFp, $cleanData);
        }

        fclose($fp);
        fclose($writeFp);

        //Delete the old file
        unlink($filename);

        //Rename the clean file to the old file
        rename('clean_' . $filename, $filename);

        $this->printToConsole("Finished!", true, true);
    }

    /**
     * Deletes all old files
     */
    public function deleteAllOldFiles()
    {
        $this->printToConsole('Delete old files.');

        if (file_exists($this->structure_filename_gz))
            unlink($this->structure_filename_gz);

        if (file_exists($this->content_filename_gz))
            unlink($this->content_filename_gz);

        if (file_exists($this->structure_filename))
            unlink($this->structure_filename);

        if (file_exists($this->content_filename))
            unlink($this->content_filename);
    }

    /**
     * Clear database tables
     */
    public function clearDatabase()
    {
        $db = Yii::$app->db;
        $this->printToConsole("Start cleaning database.", true, true);
        $db->createCommand()->truncateTable('structure')->execute();
        $db->createCommand()->truncateTable('datatypes')->execute();
        $db->createCommand()->truncateTable('content_links')->execute();
        $db->createCommand()->truncateTable('content_description')->execute();
        $this->printToConsole("Database cleaned!");
    }

    /**
     * Downloads DMOZ files
     */
    public function downloadFiles()
    {
        $this->printToConsole("Start downloading structure filename.");
        file_put_contents($this->structure_filename_gz, fopen($this->download_sf_url, 'r'));

        $this->printToConsole("Start downloading content filename.");
        file_put_contents($this->content_filename_gz, fopen($this->download_cf_url, 'r'));

        $this->printToConsole("Files are downloaded.");
    }

    /**
     * Extract gzip file
     * @param $filename
     * @param $zipped_filename
     * @throws \yii\base\ErrorException
     */

    public function extractFile($filename, $zipped_filename)
    {
        $this->printToConsole("Extracting the gunzipped file...");

        //Open the downloaded gun_zip file
        if (!($file = gzopen($zipped_filename, "r")))
        {
            throw new ErrorException("I/O error! Could not open the downloaded file!");
        }


        //Open the file we write to
        if (!$open_file = fopen($filename, 'w'))
        {
            throw new ErrorException('Cannot open the file (' . $this->filename . ')');
        }

        //Write the data
        while (!gzeof($file))
        {
            $buff = gzgets($file, 8192) ;
            fputs($open_file, $buff) ;
        }

        fclose ($open_file);
        gzclose ($file);

        $this->printToConsole(strtoupper($zipped_filename) . " WAS SUCCESSFUL EXTRACTED!", true, true);
        $this->printToConsole("Filename: $filename");
    }


    /**
     * This command starts all other commands.
     * @param bool $cleanOldDataFiles
     * @param bool $cleanDatabase
     * @param bool $createExampleFiles
     */
    public function actionIndex($cleanOldDataFiles = false, $cleanDatabase = false, $createExampleFiles = false)
    {
        $this->structure_parser = new StructureParserComponent();
        $this->content_parser = new ContentParserComponent();

        if ($cleanOldDataFiles) {
            $this->printToConsole('Clean old data files enabled', true, true);
            //$this->deleteAllOldFiles();
            //$this->downloadFiles();
            //$this->extractFile($this->structure_filename, $this->structure_filename_gz);
            //$this->extractFile($this->content_filename, $this->content_filename_gz);
            //$this->cleanFile($this->structure_filename);
            //$this->cleanFile($this->content_filename);

        }

        if ($createExampleFiles) {
            $this->printToConsole('Create example files enabled files enabled', true, true);
            $this->createExample($this->content_filename);
            $this->createExample($this->structure_filename);
        }

        if ($cleanDatabase) {
            $this->printToConsole('Clean database enabled', true, true);
            $this->clearDatabase();
            $this->structure_parser->startParse();
            $this->content_parser->startParse();
        }


        $this->actionCreateDictionary();
    }

    /**
     * This creates dictionary from unique words
     */
    public function actionCreateDictionary()
    {
        $db = Yii::$app->db;
        $this->printToConsole("Delete all words in dictionary.");
        $db->createCommand()
            ->truncateTable('dictionary')
            ->execute();

        $words_count = 0;
        $count = (new Query())
            ->select('COUNT(id)')
            ->from('content_description')
            ->scalar();

        $from = 0;
        $limit = 100;

        while ($from < $count) {

            $descriptions = (new Query())
                ->select('description')
                ->from('content_description')
                ->offset($from)
                ->limit($limit)
                ->column();

            $trimmed_words = [];

            foreach ($descriptions as $description) {
                $description = strtolower($description);

                $words = explode(' ', $description);

                foreach ($words as $word) {
                    $term = trim($word);

                    $term = preg_replace('~[^a-zA-Z]+~', '', $term);
                    $term = StemmerComponent::Stem($term);

                    $len = strlen($term);

                    if ($len > 3 && $len < 40) {
                        $trimmed_words[] = $term;
                    }
                }

                if (count($trimmed_words) > 0) {
                    $new_trimmed_words = [];
                    $trimmed_words = array_unique($trimmed_words);

                    $exist_words_with_ids = (new Query())
                        ->select(['id', 'val'])
                        ->from('dictionary')
                        ->where(['IN', 'val', $trimmed_words])
                        ->all();

                    $exist_word_ids = ArrayHelper::getColumn($exist_words_with_ids, 'id');

                    Dictionary::updateAllCounters(['count' => 1], ['IN', 'id', $exist_word_ids]);

                    $exist_words = ArrayHelper::getColumn($exist_words_with_ids, 'val');

                    $trimmed_words = array_diff($trimmed_words, $exist_words);

                    foreach ($trimmed_words as $word) {
                        $new_trimmed_words[] = [$word];
                    }

                    $new_words_count = count($new_trimmed_words);

                    if ($new_words_count > 0) {
                        $words_count += $new_words_count;

                        $this->printToConsole("Insert words to dictionary.");
                        $this->printToConsole("New words count: $new_words_count");
                        $this->printToConsole("All words count: $words_count");

                        $db->createCommand()
                            ->batchInsert('dictionary', ['val'], $new_trimmed_words)
                            ->execute();

                    }

                    $trimmed_words = [];
                }
            }

            $from += $limit;
        }
    }

    /**
     * Create vectors
     */
    public function actionVectorization()
    {
        $dictionary = (new Query())
            ->select(['id', 'val'])
            ->from('dictionary')
            ->all();

        $dictionary = ArrayHelper::map($dictionary, 'id', 'val');


        $count = (new Query())
            ->select('COUNT(id)')
            ->from('content_description')
            ->scalar();

        $from = 0;
        $limit = 100;

        while ($from < $count) {

            $descriptions = (new Query())
                ->select(['id', 'description'])
                ->from('content_description')
                ->offset($from)
                ->limit($limit)
                ->all();

            $descriptions = ArrayHelper::map($descriptions, 'id', 'description');

            foreach ($descriptions as $id => $description) {
                $vector = [];
                $terms = [];
                $description = strtolower($description);
                $words = explode(' ', $description);

                foreach ($words as $word) {
                    $term = trim($word);

                    $term = preg_replace('~[^a-zA-Z]+~', '', $term);
                    $term = StemmerComponent::Stem($term);

                    $len = strlen($term);

                    if ($len > 3 && $len < 40) {
                        $terms[] = $term;
                    }
                }


                foreach ($terms as $term) {
                    $key = array_search($term, $dictionary);

                    if (isset($vector[$key])) {
                        $vector[$key] += 1;
                    } else {
                        $vector[$key] = 1;
                    }
                }

                $data = [];

                foreach ($vector as $dictionary_id => $word_count) {
                    $data[] = [$id, $dictionary_id, $word_count];
                }

                Yii::$app->db->createCommand()
                    ->update('content_description', ['count' => count($terms)], 'id=' . $id)
                    ->execute();


                Yii::$app->db->createCommand()
                    ->batchInsert('vector', ['content_description_id', 'dictionary_id', 'count'], $data)
                    ->execute();

            }

            $from += $limit;
        }
    }


    /**
     * Creates tf idf values for vectors
     */
    public function actionInitCoordinates()
    {
        $db = Yii::$app->db;

        $dictionary = (new Query())
            ->select(['id', 'count'])
            ->from('dictionary')
            ->all();

        $dictionary = ArrayHelper::map($dictionary, 'id', 'count');

        $vector_count = (new Query())->select('COUNT(id)')->from('vector')->scalar();

        $from = 0;
        $limit = 1000;

        while ($from < $vector_count) {

            $vectors = (new Query())
                ->select(['id', 'content_description_id', 'dictionary_id', 'count'])
                ->from('vector')
                ->offset($from)
                ->limit($limit)
                ->all();

            $description_ids = ArrayHelper::getColumn($vectors, 'content_description_id');
            $descriptions = (new Query())
                ->select(['id', 'count'])
                ->from('content_description')
                ->where(['IN', 'id', $description_ids])
                ->all();
            $descriptions = ArrayHelper::map($descriptions, 'id', 'count');

            foreach ($vectors as $vector) {
                $words_count = $descriptions[$vector['content_description_id']];
                $documents_count = $dictionary[$vector['dictionary_id']];
                $tf = $vector['count'] / $words_count;
                $idf = 1.0 / $documents_count;

                $tf_idf = $tf * $idf;
                $tf_idf_kv = pow($tf_idf, 2);

                $db->createCommand()->update('vector', [
                    'tf_idf' => $tf * $idf,
                    'tf_idf_kv' => $tf_idf_kv
                ], ['id' => $vector['id']])->execute();

            }

            $from += $limit;
        }
    }


    /**
     * Устанавливает кластер документа
     * @param $description_id
     * @param $centroid_id
     */
    private function assignContentDescriptionToCentroid($description_id, $centroid_id) {
        //$this->printToConsole('Assigning for description_id=' . $description_id . ' centroid_id' . $centroid_id);
        $this->content_descriptions[$description_id] = [$centroid_id];

    }

    /**
     * Вычисляет растояние между двумя точками
     * @param $vector1
     * @param $vector2

     * @return float
     */
    private function calculateDistance($vector1, $vector2){

        //$this->printToConsole('Start calculating distance');

        $vector1 = ArrayHelper::index($vector1, 'dictionary_id');
        $vector2 = ArrayHelper::index($vector2, 'dictionary_id');

        $sum = 0;

        foreach ($vector1 as $dictionary_id => $value) {

            if (isset($vector2[$dictionary_id])) {
                $sum += pow($value['tf_idf'] - $vector2[$dictionary_id]['tf_idf'], 2);
                unset($vector2[$dictionary_id]);
            } else {
                $sum += $value['tf_idf_kv'];
            }

        }

        foreach ($vector2 as $dictionary_id => $value) {
            $sum += $value['tf_idf_kv'];
        }

        $sum = sqrt($sum);

        //$this->printToConsole('End calculating distance');

        return $sum;
    }



    /**
     * Вычисляет к какому кластеру отнести документ
     * @param $centroids_count
     */
    private function detectClusters($centroids_count) {
        $this->printToConsole('Detecting clusters start');

        $this->content_descriptions = [];

        $centroids = ArrayHelper::index(
            (new Query())->from('centroid_cord')->all(),
            null,
            'centroid_id'
        );

        $vectors = ArrayHelper::index(
            (new Query())->select(['dictionary_id', 'tf_idf', 'tf_idf_kv', 'content_description_id'])->from('vector')->all(),
            null,
            'content_description_id'
        );

        foreach ($vectors as $description_id => $vector) {

            $distance = null;

            for ($centroid_id = 1; $centroid_id <= $centroids_count; ++$centroid_id) {
                $centroid = $centroids[$centroid_id];
                $new_distance = $this->calculateDistance($centroid, $vector);

                if (($new_distance < $distance) || is_null($distance)) {
                    $distance = $new_distance;
                    $this->assignContentDescriptionToCentroid($description_id, $centroid_id);
                }
            }
        }

        Yii::$app->db->createCommand()
            ->truncateTable('content_centroid')
            ->execute();

        Yii::$app->db->createCommand()
            ->batchInsert('content_centroid', ['centroid_id'], $this->content_descriptions)
            ->execute();

    }

    /**
     * Инициализирует кластеры очищая старые таблицы и значения
     * @param $clusters_count
     */
    private function initClusters($clusters_count)
    {
        $this->printToConsole('Init clusters. Clusters count ' . $clusters_count);

        $db = Yii::$app->db;

        $db->createCommand()
            ->truncateTable('centroid')
            ->execute();

        $db->createCommand()
            ->truncateTable('centroid_cord')
            ->execute();

        for ($cluster_index = 1; $cluster_index <= $clusters_count; $cluster_index++) {
            $index = rand(1, 80000);

            $vectors = (new Query())
                ->select(['dictionary_id', 'tf_idf', 'tf_idf_kv'])
                ->from('vector')
                ->where(['content_description_id' => $index])
                ->all();

            $new_vectors = [];
            foreach ($vectors as $vector) {
                $new_vectors[] = [
                    'centroid_id' => $cluster_index,
                    'dictionary_id' => $vector['dictionary_id'],
                    'tf_idf' => $vector['tf_idf'],
                    'tf_idf_kv' => $vector['tf_idf_kv']
                ];
            }

            $db->createCommand()
                ->insert('centroid', ['name' => 'c' . $cluster_index])
                ->execute();

            $db->createCommand()
                ->batchInsert('centroid_cord', ['centroid_id', 'dictionary_id', 'tf_idf', 'tf_idf_kv'], $new_vectors)
                ->execute();

        }
    }


    public function actionKvadrat() {
        $vectors = (new Query())
            ->select(['id', 'tf_idf'])
            ->from('vector')
            ->all();

        foreach ($vectors as $vector) {
            Yii::$app->db->createCommand()->update('vector', [
                'tf_idf_kv' => pow($vector['tf_idf'], 2)],
                ['id' => $vector['id']
            ])->execute();
        }
    }

    /**
     * Вычисления новых центроидов
     * @param $centroids_count
     * @return bool
     */
    public function recalculateCentroids($centroids_count) {
        $this->printToConsole('Recalculate centroid cords');

        $centroids = ArrayHelper::index(
            (new Query())->from('centroid_cord')->all(),
            null,
            'centroid_id');


        $centroids_changed = false;

        for ($centroid_index = 1; $centroid_index <= $centroids_count; ++$centroid_index) {

            $description_ids = (new Query())
                ->select('content_description_id')
                ->from('content_centroid')
                ->where(['centroid_id' => $centroid_index])
                ->column();

            $descriptions_count = count($description_ids);

            $description_vectors = (new Query())
                ->select(['dictionary_id', 'tf_idf'])
                ->from('vector')
                ->where(['IN', 'content_description_id', $description_ids])
                ->all();

            $new_cords = [];

            foreach ($description_vectors as $description_vector) {
                $dictionary_id = $description_vector['dictionary_id'];

                if (!isset($new_cords[$dictionary_id])) {
                    $new_cords[$dictionary_id] = 0;
                }

                $new_cords[$dictionary_id] += $description_vector['tf_idf'];
            }


            $new_centroid = [];

            foreach ($new_cords as $dictionary_id => $tf_idf) {

                if ($tf_idf > self::MIN_EXP) {
                    $tf_idf = $tf_idf / $descriptions_count;
                    $new_centroid[] = [
                        'dictionary_id' => $dictionary_id,
                        'tf_idf' => $tf_idf,
                        'tf_idf_kv' => pow($tf_idf, 2)
                    ];
                } else {
                    $this->printToConsole($tf_idf);
                }
            }

            $centroid = $centroids[$centroid_index];

            if ($this->calculateDistance($new_centroid, $centroid) > 0) {
                $centroids_changed = true;

                $new_centroid_cords_array = [];
                foreach ($new_centroid as $new_centroid_cords) {
                    $new_centroid_cords_array[] = [
                        $centroid_index,
                        $new_centroid_cords['dictionary_id'],
                        $new_centroid_cords['tf_idf'],
                        $new_centroid_cords['tf_idf_kv']
                    ];
                }

                $this->printToConsole('Start deleting centroid =>' . $centroid_index);

                Yii::$app->db
                    ->createCommand()
                    ->delete('centroid_cord', [
                        'centroid_id' => $centroid_index
                    ])->execute();

                $this->printToConsole('Start adding  new cords count=>' . count($new_centroid_cords_array));

                Yii::$app->db
                    ->createCommand()
                    ->batchInsert('centroid_cord', ['centroid_id', 'dictionary_id', 'tf_idf', 'tf_idf_kv'], $new_centroid_cords_array)
                    ->execute();
            }
        }
        return $centroids_changed;
    }

    /**
     * @param $clusters_count
     * @param $iteration_count
     */
    public function actionClustering($clusters_count, $iteration_count) {
        $this->initClusters($clusters_count);
        $centroidsChange = true;

        while ($iteration_count > 0 && $centroidsChange) {
            $this->detectClusters($clusters_count);
            $centroidsChange = $this->recalculateCentroids($clusters_count);
            $this->printToConsole('Iteration: ' . $iteration_count);
            --$iteration_count;
        }
    }


    /**
     * The DMOZ files are very big size and it takes very long time to open whole file
     * This function helps to create small file from its part
     * @param $filename
     * @param int $iterations
     */
    public function createExample($filename, $iterations = 500)
    {
        $this->printToConsole("Creating example file example.$filename");
        $fp = fopen($filename, 'r');
        $writeFP = fopen('example.' . $filename , 'w');

        //Continue until you have reached end of file
        while ($iterations > 0)
        {
            $data = fgets($fp, 8192);
            fwrite($writeFP, $data);
            --$iterations;
        }

        //Close the pointers
        fclose($fp);
        fclose($writeFP);

        $this->printToConsole("Successfully create file example.$filename", true, true);
    }
}
