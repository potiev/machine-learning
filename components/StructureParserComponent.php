<?php

namespace app\components;

use yii\base\ErrorException;
use Yii;
use yii\helpers\ArrayHelper;

class StructureParserComponent extends ParserComponent
{
    public $count_rows;
    public $count_rows_temp;

    public  $current_tag;
    public  $permitted_tags;

    public  $topic;
    public  $catid;
    public  $title;
    public  $description;

    public  $type;
    public  $resource;

    public $structure_array = [];
    public $types_array = [];


    public function startParse()
    {
        $this->printToConsole("Start parsing structure.rdf.u8 file");

        $this->xml_file = 'data/structure.rdf.u8';
        $this->setStartTime();

        //Starts with clean properties
        $this->current_tag = '';
        $this->topic = '';
        $this->catid = '';
        $this->title = '';
        $this->description = '';
        $this->type = '';
        $this->resource = '';

        //
        // Here we specify what tags are legal
        //
        $this->permitted_tags = [
            'narrow',
            'narrow1',
            'narrow2',
            'symbolic',
            'symbolic1',
            'symbolic2',
            'related',
            'newsgroup'
        ];

        $this->_startToParse();

        //Print out that it is finished!
        $this->printToConsole("Finished processing structure RDF file!");
        $this->printToConsole("Inserted rows into the database: $this->count_rows");
    }

    /**
     * Function that processes the start tags.
     * @param object $__parser What parser is it dude? heh
     * @param string $__tag_name The name of the current tagname
     * $param array $__attributes Attributes of the tag_name
     **/
    public function _startTagProcessor($__parser, $__tag_name, $__attributes)
    {
        //Sets what tag we currenly are in
        $this->current_tag = $__tag_name;

        //Check if the current tag is topic
        if (strtolower($this->current_tag) == 'topic')
        {
            //If it's true get id
            $this->topic = $__attributes['r:id'];
        }

        //Check if the tag is equal to some of our permitted tags
        if (ArrayHelper::isIn(strtolower($this->current_tag), $this->permitted_tags))
        {
            //Set type to be equal with the name
            $this->type = $__tag_name;

            //Set the resource to be equal the resource found in the tag
            $this->resource = $__attributes['r:resource'];
        }
    }

    /**
     * This is our end tag processor.
     * @param object $__parser What parser is it dude? heh
     * @param string $__tag_name The name of the current tag name
     **/
    public function _endTagProcessor($parser, $__tag_name)
    {
        if (strpos($this->topic, "Top/Science") === FALSE) return;

        $db = Yii::$app->db;

        //Check if the end tag is topic, if it is it run a SQL query
        if (strtolower($__tag_name) == 'topic')
        {

            $this->structure_array[] = [
                $this->catid,
                $this->topic,
                trim($this->title),
                trim($this->description)
            ];

            $this->count_rows++; //Count rows
            $this->count_rows_temp++; //Temporary count rows - used to make a milestone

            //Reset the tags
            $this->catid = '';
            $this->topic = '';
            $this->title = '';
            $this->description = '';
            $this->current_tag = '';

            if (count($this->structure_array) == $this->batchInsertRowsCount) {
                $db->createCommand()->batchInsert(
                    'structure',
                    [
                        'catid',
                        'name',
                        'title',
                        'description'
                    ],
                    $this->structure_array
                )->execute();
                $this->structure_array = [];
            }
        }

        //Check if the end tag is something else
        if (ArrayHelper::isIn(strtolower($this->current_tag), $this->permitted_tags))
        {
            $this->types_array[] = [
                $this->catid,
                $this->type,
                trim($this->resource)
            ];

            $this->count_rows++; //Count rows
            $this->count_rows_temp++; //Temporary count rows - used to make a milestone

            $this->type = '';
            $this->resource = '';
            $this->current_tag = '';

            if (count($this->types_array) == $this->batchInsertRowsCount) {
                $db->createCommand()->batchInsert(
                    'datatypes',
                    [
                        'catid',
                        'type',
                        'resource'
                    ],
                    $this->types_array
                )->execute();

                $this->types_array = [];
            }

        }

        //Check if the stats are set
        if (ECHO_STATS) {
            //Check if ECHO_STATS_FREQUENCY is reached
            if ($this->count_rows_temp >= ECHO_STATS_FREQUENCY)
            {
                $this->count_rows_temp = 0;
                $this->_echoStatus($this->start_time, $this->count_rows, 'Yet another '.ECHO_STATS_FREQUENCY.' rows reached! - structure RDF document');
            }
        }

    }


    public function _charDataProcessor($__parser, $__data)
    {
        //Checks if there is something between the tags
        if (trim($__data) != '')
        {
            //Finds out what kind of data it is
            switch ($this->current_tag) {
                case 'catid':
                    $this->catid = $__data;
                    break;
                case 'd:Title':
                    $this->title = $__data;
                    break;
                case 'd:Description':
                    $this->description = $__data;
                    break;
                default:
                    break;
            }
        }
    }
}