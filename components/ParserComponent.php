<?php

namespace app\components;

use yii\base\ErrorException;

class ParserComponent extends BasicComponent
{
    public $batchInsertRowsCount = 500;
    public  $xml_parser;
    public  $xml_file;
    public  $start_time;

    public function setStartTime() {
        $this->start_time =  time();
    }

    /**
     * A methods that prints out the status
     * @param int $__start_time When was the script started
     * @param int $__count_rows How many rows have we inserted so far
     * @param int $__milestone A text that tells a little about our milestone
     **/
    public function _echoStatus($__start_time, $__count_rows, $__milestone)
    {
        $end_time = time();
        $this->start_time = $end_time;
        $seconds = $end_time - $__start_time;

        $this->printToConsole("$__milestone", true, true);
        $this->printToConsole("Run-time for the script: $seconds seconds");
        $this->printToConsole("Rows inserted: $__count_rows");
        flush();
    }


    /**
     * FUNCTION
     *		A method that creates the PHP's parsers and starts parsing.
     * USED BY
     *		startParse (XMLContentParser and XMLStructureParser)
     **/
    public  function _startToParse()
    {
        $this->xml_parser = xml_parser_create();
        xml_set_object($this->xml_parser, $this);

        //Parser option: Skip whitespace
        xml_parser_set_option($this->xml_parser, XML_OPTION_SKIP_WHITE, true);

        //Parser option: Case folding off
        xml_parser_set_option($this->xml_parser, XML_OPTION_CASE_FOLDING, false);

        //Set callback functions
        xml_set_element_handler($this->xml_parser, "_startTagProcessor", "_endTagProcessor");
        xml_set_character_data_handler($this->xml_parser, "_charDataProcessor");

        //Read XML file
        if (!($fp = fopen($this->xml_file, 'r')))
        {
            throw new ErrorException("File I/O error: $this->xml_file");
        }

        //Parse XML
        while ($data = fread($fp, 8192))
        {
            //error... :(
            if (!xml_parse($this->xml_parser, $data, feof($fp)))
            {
                $ec = xml_get_error_code($this->xml_parser);
                throw new ErrorException("XML parser error (error code $ec): " . xml_error_string($ec) .  "The error was found on line: " . xml_get_current_line_number($this->xml_parser));
            }
        }

        xml_parser_free($this->xml_parser);
    }
}