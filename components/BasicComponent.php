<?php
namespace app\components;

use yii\base\Component;

class BasicComponent extends Component
{
    public $count = 0;
    public $frequency = 15000;

    public function printToConsole($string, $addNewStringToEnd = true, $addNewStringToStart = false)
    {
        if ($addNewStringToStart) {
            $string = "\n$string";
        }

        if ($addNewStringToEnd) {
            $string = "$string\n";
        }

        echo ($string);
    }

    public function printDot()
    {
        ++$this->count;

        if ($this->count == $this->frequency) {
            $this->printToConsole('.', false);
            $this->count = 0;
        }
    }
}
